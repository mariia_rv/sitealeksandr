import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SimpleSlider from './SimpleSlider.js';

ReactDOM.render(
  <SimpleSlider />,
  document.getElementById('slider1')
);
