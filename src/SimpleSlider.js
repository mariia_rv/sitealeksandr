import React from "react";
import ReactDOM from 'react-dom';
import Slider from "react-slick";
import "./index.css";

export default class SimpleSlider extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings}>
        <div className="slides slide_first">
          <div>
           <div className="service"><a href="#"><h3>Название блока</h3>Описание первого блока. Краткое описание, чтоб человек понял, что ему нужен именно этот блок с информацией.</a></div>
           <button><a href="#">Read more</a></button>
          </div>
        </div>
        <div className="slides slide_second">
          <div>
           <div className="service"><a href="#"><p>Название блока</p>Описание первого блока. Краткое описание, чтоб человек понял, что ему нужен именно этот блок с информацией.</a></div>
           <button><a href="#">Read more</a></button>
          </div>
        </div>
        <div className="slides slide_third">
          <div>
           <div className="service"><a href="#"><p>Название блока</p>Описание первого блока. Краткое описание, чтоб человек понял, что ему нужен именно этот блок с информацией.</a></div>
           <button><a href="#">Read more</a></button>
          </div>
        </div>
        <div className="slides slide_fourth">
          <div>
           <div className="service"><a href="#"><p>Название блока</p>Описание первого блока. Краткое описание, чтоб человек понял, что ему нужен именно этот блок с информацией.</a></div>
           <button><a href="#">Read more</a></button>
          </div>
        </div>
      </Slider>
    );
  }
}
